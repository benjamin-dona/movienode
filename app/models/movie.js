const { Schema, model } = require("mongoose")
const MovieSchema = new Schema({
    Title: {
      type: String,
      required: true, 
      unique: true
    },
    Year: String,
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: [
        {
          Source: String,
          Value: String,
        }
    ],
})



const Movie = model("movie", MovieSchema, "movie")

module.exports = Movie
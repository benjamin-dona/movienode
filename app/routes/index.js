/* eslint-disable global-require */
const path = require('path');
const fs = require('fs');
const Router = require('koa-router');
const router = new Router();
const compose = require('koa-compose')

const glob = require('glob')
const { resolve } = require('path')
 
registerRouter = () => {
    let routers = [];
    glob.sync(resolve(__dirname, './', '**/*.js'))
        .filter(value => (!value.includes('index.js') && !value.includes('spec.js')))
        .map(router => {
            routers.push(require(router).routes())
            routers.push(require(router).allowedMethods())
        })
    return compose(routers)
      }

module.exports = registerRouter;

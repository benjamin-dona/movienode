const Router = require('koa-router');
const config = require('../config/index');
const BASE_PATH = config.basePath;
const API_V1 = config.versionNumber;
const router = new Router({prefix: `${BASE_PATH}${API_V1}`});

var compose = require('koa-compose');
const ValidHaveSearchMiddleware = require('../middlewares/valid-have-search-middleware');
const ValidBodyReplaceMiddleware = require('../middlewares/valid-body-replace-middleware');
const {
  MoviesGetFromApiMiddleware,
  MoviesGetMiddleware,
  MoviesReplaceMiddleware
} = require('../middlewares/movies-controllers-middleware');


router.get('/movie/omdbapi',
  compose([
    ValidHaveSearchMiddleware,
    MoviesGetFromApiMiddleware
  ])
);

router.get('/movie',
  compose([
    MoviesGetMiddleware
  ])
);


router.post('/movie',
  compose([
    ValidBodyReplaceMiddleware,
    MoviesReplaceMiddleware
  ])
);

module.exports = router;


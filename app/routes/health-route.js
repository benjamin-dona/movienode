const Router = require('koa-router');
const AppDetails = require(__dirname + '/../../package.json')
const config = require('../config/index');
const BASE_PATH = config.basePath;
const API_V1 = config.versionNumber;
const router = new Router({prefix: `${BASE_PATH}${API_V1}`});


router.get('/health', async (ctx) => {
  ctx.status = 200;
  ctx.type = 'json';
  ctx.body = {name: AppDetails.name, version: AppDetails.version};
});



module.exports = router;



const Joi = require('@hapi/joi')

module.exports = Joi.object({
  movie: Joi.string().required(),
  find: Joi.string().required(),
  replace: Joi.string().required(),
}).unknown();


var mockery = require('mockery');
var sinon = require("sinon");
var httpMocks = require('node-mocks-http');
const expect = require('chai').expect

describe('movies-controllers-middleware', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../config/index', {
      basePath: "any",
      versionNumber: "other"
    });

    mockery.registerMock('../loggers/logger', {
      info: () => {},
      error: () => {},
      debug: () => {}
    });

  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });
  describe('MoviesGetFromApiMiddleware', () => {

    it('should response 200', function () {
      mockery.registerMock('../controllers/movies-controller',
        class Dummy {
          async getMovieFromApi() {
            return {}
          }
        }
      );
      var nextSpy = sinon.spy();
      const {MoviesGetFromApiMiddleware} = require('./movies-controllers-middleware');
      var res = httpMocks.createResponse();

      MoviesGetFromApiMiddleware({}, res, nextSpy);
      expect(nextSpy.calledOnce).to.be.false;
      expect(res.statusCode).to.be.equal(200);
    });


  });

  describe('MoviesGetMiddleware ', () => {
    it('should response 200', function () {
      mockery.registerMock('../controllers/movies-controller',
        class Dummy {
          async getMovie() {
            return {}
          }
        }
      );
      var nextSpy = sinon.spy();
      const {MoviesGetMiddleware } = require('./movies-controllers-middleware');
      var res = httpMocks.createResponse();

      MoviesGetMiddleware({}, res, nextSpy);
      expect(nextSpy.calledOnce).to.be.false;
      expect(res.statusCode).to.be.equal(200);
    });

  });

  describe('MoviesReplaceMiddleware', () => {
    it('should response 200', function () {
      mockery.registerMock('../controllers/movies-controller',
        class Dummy {
          async movieRemplace() {
            return {}
          }
        }
      );
      var nextSpy = sinon.spy();
      const {MoviesReplaceMiddleware} = require('./movies-controllers-middleware');
      var res = httpMocks.createResponse();

      MoviesReplaceMiddleware({}, res, nextSpy);
      expect(nextSpy.calledOnce).to.be.false;
      expect(res.statusCode).to.be.equal(200);
    });
  });


});

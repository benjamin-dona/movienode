var mockery = require('mockery');
var sinon = require("sinon");
var httpMocks = require('node-mocks-http');
const expect = require('chai').expect

describe('ValidBodyReplaceMiddleware', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../config/index', {
      basePath: "any",
      versionNumber: "other",
      logger: {
        level: ''
      }
    });

    mockery.registerMock('../loggers/logger', {
      info: () => {},
      error: () => {},
      debug: () => {}
    });

  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it('should response 200', () => {
    var nextSpy = sinon.spy();
    const mockRequest = httpMocks.createRequest({
      request:{
        body:{
          movie: 'get',
          find: 'get',
          replace: 'get',
        }
      }
    });
    middleware = require('./valid-body-replace-middleware');
    var res = httpMocks.createResponse();

    middleware(mockRequest, nextSpy);
    expect(nextSpy.calledOnce).to.be.true;
    expect(nextSpy.firstCall.args.length).to.be.equal(0);
  });

  it('should next whit error when not have body',  () => {
    var nextSpy = sinon.spy();
    const mockRequest = httpMocks.createRequest({
      request:{
        body:{
        }
      }
    });
    middleware = require('./valid-body-replace-middleware');
    var res = httpMocks.createResponse();
    try{
      middleware(mockRequest, res, nextSpy);
    }catch(e){
      expect(e.code).to.equal('FORMATO_NO_PERMITIDO');
    }
  });

  it('should next whit error when not have movie',  () => {
    var nextSpy = sinon.spy();
    const mockRequest = httpMocks.createRequest({
      request:{
        body:{
          find: 'get',
          replace: 'get',
        }
      }
    });
    middleware = require('./valid-body-replace-middleware');
    var res = httpMocks.createResponse();
    try{
      middleware(mockRequest, res, nextSpy);
    }catch(e){
      expect(e.code).to.equal('FORMATO_NO_PERMITIDO');
    }
  });
});

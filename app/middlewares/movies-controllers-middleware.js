const _ = require('lodash');
const MoviesController = require('../controllers/movies-controller');
const ResponseUtil = require('../utils/response-utils');


const MoviesGetFromApiMiddleware = async (ctx, next) => {
  const controller = new MoviesController();
  const result = await controller.getMovieFromApi(
    _.get(ctx, 'query', null)
  );
  ResponseUtil.setResponse(ctx, result);
};

const MoviesGetMiddleware = async (ctx, next) => {
  const controller = new MoviesController();
  const result = await controller.getMovie({
      ..._.get(ctx, 'query', {}),
      ..._.get(ctx, 'header', {})
  });
  ResponseUtil.setResponse(ctx, result);
};


const MoviesReplaceMiddleware = async (ctx, next) => {
  const controller = new MoviesController();
  const result = await controller.movieRemplace(
    _.get(ctx, 'request.body', null)
  );
  ResponseUtil.setResponse(ctx, result);
};

module.exports = {
  MoviesGetFromApiMiddleware,
  MoviesGetMiddleware,
  MoviesReplaceMiddleware
};

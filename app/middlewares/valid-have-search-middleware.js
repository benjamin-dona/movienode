const _ = require('lodash');
const {
  BadRequestError
} = require('../utils/errors/index')


const ValidHaveSearchMiddleware = async (ctx, next) => {
  if(!_.get(ctx, 'query.search', null)){
    throw new BadRequestError('SEARCH_NOT_FOUND', 'El parametro search es obligatorio');
  }
  await next()
};

module.exports = ValidHaveSearchMiddleware;

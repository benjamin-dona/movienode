const _ = require('lodash');
const schema = require('../schemas/move-replace')
const {
  BadRequestError
} = require('../utils/errors/index')


const ValidBodyReplaceMiddleware = async (ctx, next) => {
  if (_.isEmpty(_.get(ctx, 'request.body', {}))) {
    throw new BadRequestError('FORMATO_NO_PERMITIDO', 'Se enviaron parámetros con formato erróneos');
  }

  const result = schema.validate(_.get(ctx, 'request.body', null));
  if (result.error) {
    const errorDetails = result.error.details.map(value => {
      return {
        details: value.message,
        path: value.path
      };
    });
    throw new BadRequestError('FORMATO_NO_PERMITIDO', 'Se enviaron parámetros con formato erróneos', errorDetails);
  }
  await next()
};

module.exports = ValidBodyReplaceMiddleware;

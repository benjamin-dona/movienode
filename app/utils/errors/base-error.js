class BaseError extends Error {
  constructor(status, code, message, name, metadata) {
    super(message);
    // Ensure the name of this error is the same as the class name
    this.name = name;
    this.status = status;
    this.code = code;
    this.metadata = metadata;
    // This clips the constructor invocation from the stack trace.
    // It's not absolutely essential, but it does make the stack trace a little nicer.
    //  @see Node.js reference (bottom)
    Error.captureStackTrace(this, this.constructor);
  }
}
module.exports = BaseError;

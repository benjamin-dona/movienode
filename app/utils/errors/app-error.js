const BaseError = require('./base-error');

class BadRequestError extends BaseError {
  constructor(code, message, metadata) {
    // eslint-disable-next-line no-this-before-super
    const name = BadRequestError.name;
    super(500, code, message, name, metadata);
  }
}


module.exports = BadRequestError;

const BadRequestError = require('./bad-request-error');
const AppError = require('./app-error');

module.exports = {
  BadRequestError,
  AppError
};

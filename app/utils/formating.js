const moment = require('moment');

const strDateTimeToDate = (strDateTime) => {
  if (!strDateTime || strDateTime === ''){
    return
  }
  return moment(new Date(strDateTime)).format("YYYY-MM-DD");
}

const dateTimeToDateTimeStr = (strDateTime) => {
  if (!strDateTime || strDateTime === ''){
    return
  }
  return moment(new Date(strDateTime)).format("YYYY-MM-DDTHH:mm:ss");
}


module.exports = {
  strDateTimeToDate,
  dateTimeToDateTimeStr
};

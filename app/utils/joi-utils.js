const Joi = require('@hapi/joi')
const validator = require('./validator-utils')

module.exports = Joi.extend(joi => ({
  base: joi.string(),
  type: 'util',
  messages: {
    'util.isRut': 'Needs to be a string with this format 11111111-1 and have do a valid rut'
  },
  rules: {
    isRut: {
      validate(value, helpers) {
        if (!validator.isRut(value)) {
          return helpers.error('util.isRut');
        }
        return value
      },
    }
  },
}));

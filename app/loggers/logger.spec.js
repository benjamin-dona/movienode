var mockery = require('mockery');
var sinon = require('sinon');
const expect = require('chai').expect // Using Expect style

describe("Logger", () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it("is ok ", () => {
    const pasa = []
    mockery.registerMock('winston', {
      createLogger: (param, param2) => {
        return {
          log: (param, param2) => {
            pasa.push('log')
          },
          error: (param, param2) => {
            pasa.push('error')
          },
          warn: (param, param2) => {
            pasa.push('warn')
          },
          verbose: (param, param2) => {
            pasa.push('verbose')
          },
          info: (param, param2) => {
            pasa.push('info')
          },
          debug: (param, param2) => {
            pasa.push('debug')
          },
          silly: (param, param2) => {
            pasa.push('silly')
          },
        }
      },
      format: {
        combine: (param, param2) => {},
        timestamp: (param, param2) => {},
        json: (param, param2) => {},
      },
      transports: (param, param2) => {},
    });
    const logger = require('./logger');
    logger.log('log')
    logger.error('error')
    logger.warn('warn')
    logger.verbose('verbose')
    logger.info('info')
    logger.debug('debug')
    logger.silly('silly')
    expect(pasa).to.deep.equal([
      'log',
      'error',
      'warn',
      'verbose',
      'info',
      'debug',
      'silly',
    ]);

  });

  it("other configuration ", () => {
    const pasa = []
    mockery.registerMock('../config/index', {
      logger:{
        console: 'true',
        file: 'true',
        level: 'info',
        path: '',
      }
    })
    mockery.registerMock('winston-daily-rotate-file', class Dummy{
    })
    const Console = class Dummy{}
    mockery.registerMock('winston', {
      createLogger: (param, param2) => {
        return {
          log: (param, param2) => {
            pasa.push('log')
          },
          error: (param, param2) => {
            pasa.push('error')
          },
          warn: (param, param2) => {
            pasa.push('warn')
          },
          verbose: (param, param2) => {
            pasa.push('verbose')
          },
          info: (param, param2) => {
            pasa.push('info')
          },
          debug: (param, param2) => {
            pasa.push('debug')
          },
          silly: (param, param2) => {
            pasa.push('silly')
          },
        }
      },
      format: {
        combine: (param, param2) => {},
        timestamp: (param, param2) => {},
        json: (param, param2) => {},
      },
      transports: {
        Console
      },
    });
    const logger = require('./logger');
    logger.log('log')
    logger.error('error')
    logger.warn('warn')
    logger.verbose('verbose')
    logger.info('info')
    logger.debug('debug')
    logger.silly('silly')
    expect(pasa).to.deep.equal([
      'log',
      'error',
      'warn',
      'verbose',
      'info',
      'debug',
      'silly',
    ]);

  });

});

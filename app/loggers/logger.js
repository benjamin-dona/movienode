const _ = require('lodash');
const winstonLogger = require('./logger-config');
const env = process.env.NODE_ENV || 'local';
const NAME_APP = process.env.NAME_APP || 'api-gve';

// const getTraceIds = () => {
//   const requestId = httpContext.get('requestId');
//   const traceId = httpContext.get('traceId');
//   return _.omitBy({
//     requestId,
//     traceId
//   }, _.isNull)
// }
const getLineFile = (file) => {
  const paths = file.split('/').pop()
  const elements = paths.split(':')
  return {
    file: elements[0].replace('.js', ''),
    line: elements[1]
  }
}

const getClassMethod = (classMethodName) => {
  const classMethod = classMethodName.split('.')
  if(classMethod[0] != 'Object'){
    return {
      class: classMethod[0],
      method: classMethod[1]
    }
  }
  return {
    method: classMethod[1]
  }
}
const getTrace = () => {
  try{
    throw new Error('test')
  }catch (e){
    const stack = e.stack.split('Message')
    const lineOfCall = stack[1].split('at')[2].trim().split(' ');
    let trace;
    trace = getLineFile(lineOfCall[0])
    if (lineOfCall.length  > 1){
      trace = getLineFile(lineOfCall[1].replace('(', '').replace(')', ''))
      trace = {...trace, ...getClassMethod(lineOfCall[0])}
    }
    return trace
  }
}
const formatMessage = (message, metatada) => {
  const trace = getTrace()
  // const traceIds = getTraceIds();
  return {
    ...{ appName: NAME_APP},
    ...trace,
    // ...traceIds,
    env,
    message,
    ...metatada
  };
};
var logger = {
  log: function (level, message, metatada) {
    winstonLogger.log(level, formatMessage(message, metatada));
  },
  error: function (message, metatada) {
    winstonLogger.error(formatMessage(message, {stack: _.get(metatada, 'stack', null)}));
  },
  warn: function (message, metatada) {
    winstonLogger.warn(formatMessage(message, metatada));
  },
  verbose: function (message, metatada) {
    winstonLogger.verbose(formatMessage(message, metatada));
  },
  info: function (message, metatada) {
    winstonLogger.info(formatMessage(message, metatada));
  },
  debug: function (message, metatada) {
    winstonLogger.debug(formatMessage(message, metatada));
  },
  silly: function (message, metatada) {
    winstonLogger.silly(formatMessage(message, metatada));
  }
};

module.exports = logger;

const {
  createLogger,
  format,
  transports
} = require('winston');
const {
  combine,
  timestamp,
} = format;
const config = require('../config/index');


const configLog = {
  filename: config.logger.path + '/-api.log',
  level: process.env.NODE_ENV === 'test' ? [] : config.logger.level
}
let transportsLog = [];
if (config.logger.file === 'true') {
  transportsLog.push(new(require('winston-daily-rotate-file'))(configLog))
}
if (config.logger.console === 'true') {
  transportsLog.push(new transports.Console(configLog))
}
const winstonLogger = createLogger({
  transports: transportsLog,
  format: combine(
    timestamp(),
    format.json()
  ),
});


module.exports = winstonLogger;

var mockery = require('mockery');
var sinon = require('sinon');
const expect = require('chai').expect // Using Expect style

describe("MoviesController", () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger', {
      info: () => {},
      error: () => {},
      debug: () => {}
    });

    mockery.registerMock('../../config/index', {});
    mockery.registerMock('../services/movies/movies-service', class Dummy {});
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it("Should be an instance of MoviesController", () => {
    mockery.registerMock('../services/movies/movies-service',
      class Dummy {
        async getMovieFromApi(rutDeudores) {
          return {}
        }
      }
    );
    MoviesController = require('./movies-controller');
    controller = new MoviesController();
    expect(controller instanceof MoviesController).to.equal(true);;
  });

  it("should return getMovieFromApi", async () => {
    // mock del servicio de mongo
    const params = {
      search: 'the',
      year: 1,
    }

    const data = {
        "Title": "Up",
        "Year": "2009",
        "Released": "29 May 2009",
        "Genre": "Animation, Adventure, Comedy",
        "Director": "Pete Docter, Bob Peterson",
        "Actors": "Edward Asner, Jordan Nagai, John Ratzenberger",
        "Plot": "78-year-old Carl Fredricksen travels to Paradise Falls in his house equipped with balloons, inadvertently taking a young stowaway.",
        "Ratings": [
            {
                "Source": "Internet Movie Database",
                "Value": "8.2/10"
            },
            {
                "Source": "Rotten Tomatoes",
                "Value": "98%"
            },
            {
                "Source": "Metacritic",
                "Value": "88/100"
            }
        ]
    }
      mockery.registerMock('../services/movies/movies-service',
      class Dummy {
        async getMovieFromApi() {
          return data
        }
      }
    );

    MoviesController = require('./movies-controller');
    controller = new MoviesController();
    const response = await controller.getMovieFromApi(params);
    expect(response).to.deep.equal(data);
  });

  it("should return getMovie", async () => {
    // mock del servicio de mongo
    const params = {
      items: 11,
      page: 3,
    }

    const data = {
        "Title": "Up",
        "Year": "2009",
        "Released": "29 May 2009",
        "Genre": "Animation, Adventure, Comedy",
        "Director": "Pete Docter, Bob Peterson",
        "Actors": "Edward Asner, Jordan Nagai, John Ratzenberger",
        "Plot": "78-year-old Carl Fredricksen travels to Paradise Falls in his house equipped with balloons, inadvertently taking a young stowaway.",
        "Ratings": [
            {
                "Source": "Internet Movie Database",
                "Value": "8.2/10"
            },
            {
                "Source": "Rotten Tomatoes",
                "Value": "98%"
            },
            {
                "Source": "Metacritic",
                "Value": "88/100"
            }
        ]
    }
      mockery.registerMock('../services/movies/movies-service',
      class Dummy {
        async getMovie() {
          return data
        }
      }
    );

    MoviesController = require('./movies-controller');
    controller = new MoviesController();
    const response = await controller.getMovie(params);
    expect(response).to.deep.equal(data);
  });

  it("should return ok movieRemplace", async () => {
    // mock del servicio de mongo
    const params = {
      movie:'UP',
      find:'78',
      replace: '7'
    }

    const data = {
        "Title": "Up",
        "Year": "2009",
        "Released": "29 May 2009",
        "Genre": "Animation, Adventure, Comedy",
        "Director": "Pete Docter, Bob Peterson",
        "Actors": "Edward Asner, Jordan Nagai, John Ratzenberger",
        "Plot": "78-year-old Carl Fredricksen travels to Paradise Falls in his house equipped with balloons, inadvertently taking a young stowaway.",
        "Ratings": [
            {
                "Source": "Internet Movie Database",
                "Value": "8.2/10"
            },
            {
                "Source": "Rotten Tomatoes",
                "Value": "98%"
            },
            {
                "Source": "Metacritic",
                "Value": "88/100"
            }
        ]
    }
      mockery.registerMock('../services/movies/movies-service',
      class Dummy {
        async getMovieDBByTitle() {
          return data
        }
        async setMovieRemplacePlotByTitle(){
          return data
        }
      }
    );

    MoviesController = require('./movies-controller');
    controller = new MoviesController();
    const response = await controller.movieRemplace(params);
    expect(response.Plot).to.deep.equal(data.Plot);
  });
});

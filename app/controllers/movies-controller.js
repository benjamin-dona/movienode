const _ = require('lodash');
const moment = require('moment');
const MoviesService = require('../services/movies/movies-service');
const logger = require('../loggers/logger')
const {
  BadRequestError
} = require('../utils/errors')

module.exports = class MoviesController {
  async getMovieFromApi(params) {
    logger.info('Inicio')
    const filters = this.setFiltersFromApi(params);
    const moviesService = new MoviesService();
    const result = await moviesService.getMovieFromApi(filters);
    logger.info('responde')
    return result;
  }

  async getMovie(params) {
    logger.info('Inicio')
    const filters = this.setFilters(params);
    const moviesService = new MoviesService();
    const result = await moviesService.getMovie(filters);
    logger.info('responde')
    return result;
  }

  async movieRemplace(remplace) {
    logger.info('Inicio')
    const moviesService = new MoviesService();
    const movie = await moviesService.getMovieDBByTitle(remplace.movie);
    if(movie.length === 0){
      throw new BadRequestError('MOVIE_NOT_FOUND', 'The movie not found by title')
    }
    logger.info('responde')
    return await moviesService.setMovieRemplacePlotByTitle(remplace.movie, remplace);

  }
  //Private
  setFiltersFromApi(params){
    let  filter ={
      t: _.get(params, 'search'),
    }
    if(_.get(params, 'year') ){
      filter.y = _.get(params, 'year');
    }
    return filter
  }


  setFilters(params){
    let  filter ={}
    filter.offset = 0
    filter.limit = 5
    if(_.get(params, 'items', 0) > 0 ){
      filter.limit = parseInt(params.items, 10)
    }
    if(_.get(params, 'page', 0) > 0 ){
      filter.offset = (_.get(params, 'page') -1) * filter.limit ;
    }
    return filter
  }

};

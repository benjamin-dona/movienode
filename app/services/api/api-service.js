const axios = require('axios')
const _ = require('lodash');
const logger = require('../../loggers/logger')

const {
  BadRequestError,
  AppError
} = require('../../utils/errors')


const api = async (url, method= 'GET',data= null) => {
  logger.info("Inicion de envio por api")
  let option = {
    method: method,
    url: url,
  }
  if (data !== null){
    option.data= data
  }
  logger.info(option)
  return axios(option).then((response) => {
    logger.info("recibe")
    logger.info(response.data)
    return response.data
  }, (error) => {
    logger.error(error)
    switch (error.response.status) {
      case 400:
        throw new BadRequestError('ERROR_400', error.response.message,error)
      case 401:
        throw new BadRequestError('UNAUTHORIZED', error.response.data.Error,error)
      case 404:
        throw new BadRequestError('NOT_FOUND', error.response.data.Error,error)
      default:
        throw new AppError('ERROR_500', error.response.data.Error, error)
    }
  });;

}


module.exports = api;
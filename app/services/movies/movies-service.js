const _ = require('lodash');
const api = require('../api/api-service');
const config = require('../../config');
const Movie = require('../../models/movie');
const MovieSerialize = require('../../serialize/movie-serialize');

const {
  BadRequestError
} = require('../../utils/errors')


const logger = require('../../loggers/logger');

class MoviesService {


  async getMovieFromApi(filters) {
    logger.info('Inicio')
    let moviesInDB = await api(`${config.omdb.url}/?apikey=${config.omdb.key}&${new URLSearchParams(filters).toString()}`);
    if(moviesInDB.Response === 'False' ){
      throw new BadRequestError('MOVIE_NOT_FOUND', moviesInDB.Error)
    }
    moviesInDB = MovieSerialize(moviesInDB);
    const movies = await this.getMovieDBByTitle(moviesInDB.Title)
    if(movies.length === 0){
      await this.saveMovieDB(moviesInDB)
    }
    return moviesInDB
  }

  async getMovieDBByTitle(Title) {
    logger.info('Inicio')
    const data = await Movie.findOne({Title:Title})
    return data===null ? []: data;
  }

  async saveMovieDB(movie) {
    logger.info('Inicio')
    var newMovie = new Movie(movie);
    await newMovie.save();
    logger.info('Guardada')
    return movie;
  }

  async getMovie(filters) {
    logger.info('Inicio')
    let moviesInDB = await Movie.find().skip(filters.offset).limit(filters.limit)
    moviesInDB = _.map(moviesInDB, (item) => {
      return MovieSerialize(item);
    });
    logger.info('Se encuentra')
    return moviesInDB
  }

  async setMovieRemplacePlotByTitle(Title, remplace) {
    logger.info('Inicio')
    let moviesInDB = await Movie.findOne({Title:Title})
    moviesInDB.Plot=moviesInDB.Plot.replace(remplace.find, remplace.replace);
    logger.info('Se guarda')
    moviesInDB.save();
    return MovieSerialize(moviesInDB)
  }

}


module.exports = MoviesService;

var mockery = require('mockery');
var sinon = require('sinon');
const expect = require('chai').expect // Using Expect style

describe("Movie Service", () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../../loggers/logger', {
      info: (param, param2) => {},
      debug: (param) => {},
      error: (param) => {}
    });

    mockery.registerMock('../../config/index', {});
  });

  afterEach(() => {
    mockery.resetCache();
    mockery.disable();
    mockery.deregisterAll();
  });

  it("Should be an instance of MoviesService", () => {
    class DummyClass {
      async query() {
        return {}
      }
    }
    mockery.registerMock('../mssql/mssql-service', new DummyClass());
    Service = require('./movies-service');
    service = new Service();
    expect(service instanceof Service).to.equal(true);;
  });

});

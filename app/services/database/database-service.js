const mongoose = require('mongoose'); 
const config = require('../../config/')
const logger = require('../../loggers/logger');
const initDB = () => { 
  mongoose.connect(`mongodb://${config.mongodb.username}:${config.mongodb.password}@${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.database}?authSource=admin&w=1`, 
  { useNewUrlParser: true,
    useUnifiedTopology: true  });
  mongoose.connection.once('open', () => { 
    console.log('connected to database'); 
  }); 

  mongoose.connection.on('error', (err) => { 
    logger.error('Error en la conexión', err)
    process.exit()
  }); 
} 
module.exports = initDB;
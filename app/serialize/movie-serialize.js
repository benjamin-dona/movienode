const _ = require('lodash');
module.exports = (data) => {
  return _.omitBy({
    Title: _.get(data, 'Title'),
    Year: _.get(data, 'Year', null),
    Released: _.get(data, 'Released'),
    Genre: _.get(data, 'Genre', null),
    Director: _.get(data, 'Director'),
    Actors: _.get(data, 'Actors', null),
    Plot: _.get(data, 'Plot', null),
    Ratings: _.get(data, 'Ratings', null),
  }, _.isNull);
}

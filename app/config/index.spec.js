var mockery = require('mockery');
var sinon = require('sinon');
const expect = require('chai').expect // Using Expect style

describe("Config start", () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: false
    });
    mockery.registerMock('../mssql/mssql-service', {});

    mockery.registerMock('../../loggers/logger', {
      info: (param, param2) => {},
      debug: (param) => {},
      error: (param) => {}
    });

  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

    it("is ok ", async () => {
      const array1 = ['local', 'dev', 'qa', 'test', 'prod'];
      const config = require('./index');
      expect(config.basePath).to.equal('/');
      expect(config.versionNumber).to.equal('v1');
      delete require.cache[require.resolve('./index')];
      array1.forEach(element => {
        process.env.NODE_ENV = element
        const config = require('./index');
        expect(config.basePath).to.equal('/');
        expect(config.versionNumber).to.equal('v1');
        delete require.cache[require.resolve('./index')];
      });

  });
});

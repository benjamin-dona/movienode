module.exports = {
  basePath: '/',
  versionNumber: 'v1',
  name: process.env.NAME_APP || 'api',
  omdb: {
    url: process.env.OMDB_API_URL,
    key: process.env.OMDB_API_KEY,
  },
  logger: {
    console: process.env.LOG_CONSOLE || true,
    file: process.env.LOG_FILE || true,
    level: process.env.LOG_LEVEL || 'info',
    path: process.env.LOG_PATH,
  },
  mongodb:{
    username: process.env.MONGODB_USERNAME,
    password: process.env.MONGODB_PASSWORD,
    host: process.env.MONGODB_HOST,
    port: process.env.MONGODB_PORT,
    database: process.env.MONGODB_DATABASE,
  }
};

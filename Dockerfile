FROM node:12-alpine

WORKDIR /home/app/

COPY package.json ./

RUN npm install --save glob

RUN npm install --quiet --production

RUN chown node:node /home/app/

USER node

COPY --chown=node:node app app
COPY --chown=node:node app.js app.js
COPY --chown=node:node bin bin

ARG BUILDNUMBER

ENV BUILDNUMBER $BUILDNUMBER



EXPOSE 3010
CMD [ "npm", "start" ]

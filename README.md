# Proyecto Movie
Este una api que obtiene las películas de omdb y las guardas en una base de dato mongo

## Documentacion
La documentacion de la api esta en la carpeta swagger/redoc-static.html

## Local
Este proyecto se ha realizado en un computador linux, con node y se utilizo el ide vscode.

### Install dependencies:
Necesita de:
- mongoDB

### Instalar módulos node
```
 npm install
```

### Ejecutar

```
 npm start
```

### Correr en VScode
Se debe modificar el archivo de environment .env, y ejecutar el proyectos desde el ide

## Enviroment
Las variables de entornos que soporta son:
- OMDB_API_URL: Url de la api
- OMDB_API_KEY: Key de conexión con la api
- MONGODB_HOST: host de mongo
- MONGODB_PORT: puerto de mongo
- MONGODB_DATABASE: Base de dato de mongo
- MONGODB_USERNAME: User de mongo
- MONGODB_PASSWORD: Clave de mongo
- LOG_LEVEL: Nivel de sensibilidad del log
- LOG_CONSOLE:(true/false) Si quiere imprimir el log por la consola
- LOG_FILE:(true/false) Si quiere imprimir el log por el archivo
- LOG_PATH: Ubicación del archivo de log


## Docker
### Compilar 
```
docker build -t "movie:0.1.0" -f Dockerfile .
```

###  Publicar docker
```
docker run --name movie -p 6000:3010 \
-e OMDB_API_URL=http://www.omdbapi.com \
-e OMDB_API_KEY=2e57dff7 \
-e MONGODB_HOST=127.0.0.1 \
-e MONGODB_PORT=27017 \
-e MONGODB_DATABASE=movies \
-e MONGODB_USERNAME=root \
-e MONGODB_PASSWORD=pass12345 \
-e LOG_LEVEL=info \
-e LOG_CONSOLE=true \
-e LOG_FILE=false \
-e LOG_PATH=logs \
-e PORT=3010 \
-d movie:0.1.0
```

### Docker compose
Ejecutarlo como una solución completa con su propio mongo
```
docker-compose up -d
```
Se utilizan los siguientes puertos:

- 27017: Puerto de mongoDB
- 9091: mongo-expres con el user/pass->admin/admin123
- 9092: Puerto de Api movie



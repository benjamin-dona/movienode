global.Promise = require('bluebird');
const _ = require('lodash');
const Koa = require('koa');
var bodyParser = require('koa-body-parser');
const registerRouter = require('./app/routes/index');
const morgan = require('koa-morgan');
const FileStreamRotator = require('file-stream-rotator');
const logger = require('./app/loggers/logger');
const config = require('./app/config/index');
var initDB  = require('./app/services/database/database-service');
const uuid = require('uuid');
initDB()

const app = new Koa();
app.use( async (ctx, next) => {
  requestId = _.get(ctx, 'headers.request-id',uuid.v1());
  traceId = _.get(ctx, 'headers.trace-id',null);
  if(requestId){
    ctx.set('requestId',requestId);
  }
  if(traceId){
    ctx.set('traceId', traceId);
  }
  await next();
});

const logOptions = () => {
  const logStream = FileStreamRotator.getStream({
    date_format: 'YYYY-MM-DD',
    filename: 'logs/access-%DATE%.log',
    frequency: 'daily',
    verbose: false,
  });
  return {
    stream: logStream
  };
};

app.use(bodyParser());

const mapErrorResponse = (err) => {
  let codeMsg = 'ERROR_500';
  if (err.code !== undefined) {
    codeMsg = err.code;
  }
  // eslint-disable-next-line prefer-const
  let object = {
    codigo: codeMsg,
    mensaje: err.message
  };
  if (err.metadata) {
    object.metadata = err.metadata;
  }
  logger.error(`${err.message}`, err);
  return object;
};

const logFormat = process.env.LOG_FORMAT || 'dev';
app.use(morgan(logFormat, logOptions()));

app.use( registerRouter());

app.use(async function(ctx, next) {
  try {
    await next();
  } catch (err) {
    let statusCode = 500;
    if (err.status !== undefined) {
      statusCode = err.status;
    }
    const errorResponse = mapErrorResponse(err);
    ctx.status = statusCode;
    ctx.type = 'json';
    ctx.body = errorResponse;

    // since we handled this manually we'll
    // want to delegate to the regular app
    // level error handling as well so that
    // centralized still functions correctly.
    ctx.app.emit('error', err, ctx);
  }
});
app.on('error', function(err, ctx) {
  if (process.env.NODE_ENV != 'test') {
    console.log('sent error %s to the cloud', err.message);
    console.log(err);
  }
  if (!ctx || ctx.headerSent || !ctx.writable) return

  ctx.writable = false //  interrupt `ctx.onerror`
  let statusCode = 500;
  if (err.status !== undefined) {
    statusCode = err.status;
  }
  const errorResponse = mapErrorResponse(err);
  ctx.status = statusCode;
  ctx.type = 'json';
  ctx.res.end(JSON.stringify(errorResponse))
});
// Handle 404
app.use(async function pageNotFound(ctx) {
  // we need to explicitly set 404 here
  // so that koa doesn't assign 200 on body=
  ctx.status = 404;
  logger.error(`404: Page not Found: ${ctx.originalUrl}`);
  ctx.body = {
    message: '404: Page not Found'
  };
});

module.exports = app;
